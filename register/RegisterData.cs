﻿namespace SimpeApp.register {
    class RegisterData {
        public string AccountType { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmationPassword { get; set; }
    }
}
