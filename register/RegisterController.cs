﻿using rasyidtechlib.basic;
using SimpeApp.service;
using System.Windows;

namespace SimpeApp.register {
    class RegisterController : MyController {
        private static IAuthService authService;

        public RegisterController(IMyView _myView) : base(_myView) {
        }

        public void doRegister(RegisterData registerData) {
            bool registerSuccess = isRegisterSuccess(registerData);

            if (!registerSuccess) {
                return;
            }

            if (authService.getCurrentUserInfo().getAccountType() == AccountType.KARYAWAN) {
                getView().callMethod("moveToTransactionWindow");
            } else {
                getView().callMethod("moveToCartWindow");
            }
        }

        private bool isRegisterSuccess(RegisterData registerData) {
            bool passwordMatch = isPasswordMatch(registerData.Password, registerData.ConfirmationPassword);
            bool fieldsNotEmpty = isFieldsNotEmpty(registerData);

            if (passwordMatch && fieldsNotEmpty) {
                bool registered = authService.register(registerDataToUser(registerData));
                if (!registered) {
                    MessageBox.Show("Username has already been taken", "Register Failed", MessageBoxButton.OK);
                    return false;
                }

                return true;
            }

            return false;
        }

        private bool isFieldsNotEmpty(RegisterData registerData) {
            bool usernameEmpty = registerData.Password.Trim().Equals("");
            bool emailEmpty = registerData.Email.Trim().Equals("");
            bool passwordEmpty = registerData.Password.Trim().Equals("");
            bool confirmationPasswordEmpty = registerData.ConfirmationPassword.Trim().Equals("");

            bool fieldsEmpty = usernameEmpty || emailEmpty || passwordEmpty || confirmationPasswordEmpty;
            if (fieldsEmpty) {
                MessageBox.Show("Please fill all fields", "Register Failed", MessageBoxButton.OK);
            }

            return !fieldsEmpty;
        }

        private bool isPasswordMatch(string password, string confirmationPassword) {
            if (!password.Equals(confirmationPassword)) {
                MessageBox.Show("Password confirmation doesn't match", "Register Failed", MessageBoxButton.OK);
                return false;
            }

            return true;
        }

        public static void inject(IAuthService _authService) {
            authService = _authService;
        }

        private User registerDataToUser(RegisterData registerData) {
            AccountType accountType;
            if (registerData.AccountType.Equals("Karyawan")) {
                accountType = AccountType.KARYAWAN;
            } else {
                accountType = AccountType.PELANGGAN;
            }
            return new User(registerData.Username, registerData.Email, registerData.Password, accountType);
        }
    }
}
