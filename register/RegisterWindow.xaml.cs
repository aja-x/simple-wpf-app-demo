﻿using rasyidtechlib.basic;
using rasyidtechlib.ui_element.button;
using rasyidtechlib.ui_element.combo_box;
using rasyidtechlib.ui_element.password_box;
using rasyidtechlib.ui_element.text_box;
using rasyidtechlib.validator;
using SimpeApp.login;
using SimpeApp.transaction;
using System.Windows;

namespace SimpeApp.register {
    public partial class RegisterWindow : MyWindow, IMyController {
        private IMyComboBox accountTypeCbx;
        private IMyTextBox usernameTxt;
        private IMyTextBox emailTxt;
        private IMyPasswordBox passwordPwd;
        private IMyPasswordBox confirmationpasswordPwd;
        private IMyButton loginBtn;
        private IMyButton registerBtn;

        public RegisterWindow() {
            InitializeComponent();
            init();
        }

        private void init() {
            setController(new RegisterController(this));

            accountTypeCbx = new BuilderComboBox().activate(this, "accountType").setItemList(new MyList<string> { "Pelanggan", "Karyawan" });
            usernameTxt = new BuilderTextBox().activate(this, "username");
            emailTxt = new BuilderTextBox().activate(this, "email").setValidation(new MyRegex("^[a-z0-9][-a-z0-9._]+@([-a-z0-9]+[.])+[a-z]{2,5}$"));

            passwordPwd = new BuilderPasswordBox().activate(this, "password");
            confirmationpasswordPwd = new BuilderPasswordBox().activate(this, "passwordConfirmation");

            loginBtn = new BuilderButton().activate(this, "loginButton").addOnClick(this, "onClickLoginButton");
            registerBtn = new BuilderButton().activate(this, "registerButton").addOnClick(this, "onClickRegisterButton");
        }

        public void onClickLoginButton() {
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.Show();
            Close();
        }

        public void onClickRegisterButton() {
            RegisterData registerData = new RegisterData {
                AccountType = accountTypeCbx.getSelectedItem(),
                Username = usernameTxt.getText(),
                Email = emailTxt.getText(),
                Password = passwordPwd.getPassword(),
                ConfirmationPassword = confirmationpasswordPwd.getPassword(),
            };

            getController().callMethod("doRegister", registerData);
        }

        public void moveToTransactionWindow() {
            TransactionWindow transactionWindow = new TransactionWindow();
            transactionWindow.Show();
            Close();
        }
    }
}
