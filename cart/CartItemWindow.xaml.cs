﻿using rasyidtechlib.basic;
using rasyidtechlib.ui_element.button;
using rasyidtechlib.ui_element.combo_box;
using rasyidtechlib.ui_element.text_box;
using System;

namespace SimpeApp.cart {
    public partial class CartItemWindow : MyWindow, IMyController {
        private IMyComboBox productCbx;
        private IMyTextBox priceTxt;
        private IMyTextBox countTxt;
        private IMyTextBox subTotalTxt;
        private IMyButton addBtn;
        private IMyButton cancelBtn;

        public CartItemWindow() {
            InitializeComponent();

            productCbx = new BuilderComboBox().activate(this, "product").setItemList(getItemList());
            priceTxt = new BuilderTextBox().activate(this, "price").setText("25000");
            countTxt = new BuilderTextBox().activate(this, "count").setText("2");
            subTotalTxt = new BuilderTextBox().activate(this, "subTotal").setText("50000");
            addBtn = new BuilderButton().activate(this, "addButton").addOnClick(this, "onClickAddBtn");
            cancelBtn = new BuilderButton().activate(this, "addButton").addOnClick(this, "onClickCancelBtn");
        }

        public void onClickAddBtn() {
            Close();
        }
        
        public void onClickCancelBtn() {
            Close();
        }

        private MyList<string> getItemList() {
            MyList<string> items = new MyList<string>() { "Aqua Kardus", "Tango Wafer", "Minyak Goreng" };
            return items;
        }
    }
}
