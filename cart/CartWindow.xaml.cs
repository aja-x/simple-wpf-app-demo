﻿using rasyidtechlib.basic;
using rasyidtechlib.ui_element.button;
using rasyidtechlib.ui_element.data_grid;
using rasyidtechlib.ui_element.text_box;
using SimpeApp.transaction;
using System;
using System.Windows;

namespace SimpeApp.cart {
    public partial class CartWindow : MyWindow, IMyController {
        private IMyTextBox dateTxt;
        private IMyTextBox usernameTxt;
        private IMyButton addItemBtn;
        private IMyButton deleteBtn;
        private IMyDataGrid itemDataDtg;
        private IMyTextBox totalTxt;
        private IMyButton payBtn;
        private IMyButton cancelBtn;

        public CartWindow(string userUsername = null) {
            InitializeComponent();

            setController(new CartController(this, userUsername));
            init(userUsername);
        }

        private void init(string userUsername = null) {
            dateTxt = new BuilderTextBox().activate(this, "date").setText(DateTime.Now.ToString());
            usernameTxt = new BuilderTextBox().activate(this, "username").setText((userUsername != null) ? userUsername : "");
            addItemBtn = new BuilderButton().activate(this, "addItemButton").addOnClick(this, "onClickAddItemBtn");
            deleteBtn = new BuilderButton().activate(this, "delButton");
            itemDataDtg = new BuilderDataGrid().activate(this, "itemData")
                .setColumnHeader("No", "Id").setColumnHeader("Nama Produk", "ProductName").setColumnHeader("Harga", "Price").addItemSource(getData());
            totalTxt = new BuilderTextBox().activate(this, "total").setText("100000");
            payBtn = new BuilderButton().activate(this, "payButton").addOnClick(this, "onClickPayBtn");
            cancelBtn = new BuilderButton().activate(this, "cancelButton").addOnClick(this, "onClickCancelBtn");
        }

        public void onClickPayBtn() {
            MessageBox.Show("Pembayaran telah diterima, mohon tunggu struk pembayaran", "Success", MessageBoxButton.OK);
            TransactionWindow transactionWindow = new TransactionWindow();
            transactionWindow.Show();
            Close();
        }

        public void onClickCancelBtn() {
            TransactionWindow transactionWindow = new TransactionWindow();
            transactionWindow.Show();
            Close();
        }

        private MyList<CartItem> getData() {
            MyList<CartItem> cartItems = new MyList<CartItem>();
            cartItems.Add(new CartItem { Id = 1, ProductName = "Aqua Kardus", Price = 50000 });
            cartItems.Add(new CartItem { Id = 2, ProductName = "Tango Wafer", Price = 20000 });
            cartItems.Add(new CartItem { Id = 3, ProductName = "Minyak Goreng", Price = 30000 });
            return cartItems;
        }

        public void onClickAddItemBtn() {
            CartItemWindow cartItemWindow = new CartItemWindow();
            bool? showDialog = cartItemWindow.ShowDialog();
            if (showDialog != null && showDialog == true) {
                
            }
        }
    }
}
