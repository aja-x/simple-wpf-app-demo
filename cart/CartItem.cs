﻿namespace SimpeApp.cart {
    class CartItem {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public int Price { get; set; }
    }
}
