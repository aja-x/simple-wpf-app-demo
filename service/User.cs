﻿using System;

namespace SimpeApp.service {
    public class User {
        private string username;
        private string email;
        private string password;
        private AccountType accountType;

        public User(string username, string email, string password, AccountType accountType) {
            this.username = username;
            this.email = email;
            this.password = password;
            this.accountType = accountType;
        }

        public User() {
        }

        public string getUsername() {
            return username;
        }

        public void setUsername(string value) {
            username = value;
        }

        public string getEmail() {
            return email;
        }

        public void setEmail(string value) {
            email = value;
        }

        public string getPassword() {
            return password;
        }

        public AccountType getAccountType() {
            return accountType;
        }

        public void setAccountType(AccountType value) {
            accountType = value;
        }
    }
}
