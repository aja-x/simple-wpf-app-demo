﻿using System.Collections.Generic;

namespace SimpeApp.service {
    internal class AuthService : IAuthService {
        private Dictionary<string, User> users;
        private string currentUser;
        private static IAuthService instance;

        private AuthService() {
            users = new Dictionary<string, User>();
        }

        public static IAuthService getInstance() {
            if (instance == null) {
                instance = new AuthService();
            }

            return instance;
        }

        public User getCurrentUserInfo() {
            if (currentUser != null) {
                return users[currentUser];
            }

            return null;
        }

        public bool login(string username, string password) {
            if (users.ContainsKey(username)) {
                User user = users[username];
                if (user.getPassword().Equals(password)) {
                    currentUser = username;
                    return true;
                }
            }

            return false;
        }

        public bool register(User user) {
            if (!(users.ContainsKey(user.getUsername()) || users.ContainsValue(user))) {
                users.Add(user.getUsername(), user);
                currentUser = user.getUsername();
                return true;
            }

            return false;
        }

        public bool isUserExist(string username) {
            return users.ContainsKey(username);
        }

        public void logout() {
            currentUser = null;
        }
    }
}
