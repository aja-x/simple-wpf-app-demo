﻿namespace SimpeApp.service {
    public interface IAuthService {
        User getCurrentUserInfo();
        bool login(string username, string password);
        bool register(User user);
        bool isUserExist(string username);
        void logout();
    }
}
