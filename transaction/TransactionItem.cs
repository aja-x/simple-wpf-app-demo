﻿namespace SimpeApp.transaction {
    public class TransactionItem {
        public int Id { get; set; }
        public string TransactionDate { get; set; }
        public string Customer { get; set; }
        public int Total { get; set; }
    }
}