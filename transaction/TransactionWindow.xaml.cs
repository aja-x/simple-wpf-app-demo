﻿using rasyidtechlib.basic;
using rasyidtechlib.ui_element.button;
using rasyidtechlib.ui_element.data_grid;
using rasyidtechlib.ui_element.text_box;
using SimpeApp.login;

namespace SimpeApp.transaction {
    public partial class TransactionWindow : MyWindow, IMyController {
        private IMyButton newTransactionBtn;
        private IMyButton logoutBtn;
        private IMyDataGrid transactionLogDataGrd;

        public TransactionWindow() {
            InitializeComponent();

            setController(new TransactionController(this));

            newTransactionBtn = new BuilderButton().activate(this, "newTransactionButton")
                .addOnClick(this, "onClickNewTransactionButton");
            logoutBtn = new BuilderButton().activate(this, "logoutButton")
                .addOnClick(this, "onClicklogoutButton");
            transactionLogDataGrd = new BuilderDataGrid().activate(this, "transactionLogDataGrid")
                .setColumnHeader("ID", "Id")
                .setColumnHeader("Tgl Transaksi", "TransactionDate")
                .setColumnHeader("Pelanggan", "Customer")
                .setColumnHeader("Total", "Total")
                .addItemSource(getData());
        }

        public void onClickNewTransactionButton() {
            AddTransactionWindow addTransactionWindow = new AddTransactionWindow();
            bool? showDialog = addTransactionWindow.ShowDialog();
            if (showDialog != null && showDialog == true) {
                Close();
            }
        }

        public void onClicklogoutButton() {
            getController().callMethod("doLogout");
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.Show();
            Close();
        }

        private MyList<TransactionItem> getData() {
            MyList<TransactionItem> transactionItems = new MyList<TransactionItem>();
            transactionItems.Add(new TransactionItem {
                Id = 1, Customer = "Budi", TransactionDate = "16/05/2020", Total = 100000,
            });
            transactionItems.Add(new TransactionItem {
                Id = 2, Customer = "Ani", TransactionDate = "20/05/2020", Total = 58000,
            });

            return transactionItems;
        }
    }
}
