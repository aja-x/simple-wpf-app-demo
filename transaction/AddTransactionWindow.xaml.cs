﻿using rasyidtechlib.basic;
using rasyidtechlib.ui_element.button;
using rasyidtechlib.ui_element.text_box;
using SimpeApp.cart;
using SimpeApp.service;
using System.Windows;

namespace SimpeApp.transaction {
    public partial class AddTransactionWindow : MyWindow, IMyController {
        private IMyTextBox usernameTxt;
        private IMyButton confirmationBtn;
        private static IAuthService authService;
        
        public AddTransactionWindow() {
            InitializeComponent();

            usernameTxt = new BuilderTextBox().activate(this, "username");
            confirmationBtn = new BuilderButton()
                .activate(this, "confirmationButton")
                .addOnClick(this, "onClickConfirmationButton");
        }

        public void onClickConfirmationButton() {
            if (!usernameTxt.getText().Trim().Equals("") && !authService.isUserExist(usernameTxt.getText().Trim())) {
                MessageBox.Show("Username not found", "Username Not Found", MessageBoxButton.OK);
                return;
            }

            Close();
            CartWindow cartWindow = new CartWindow(usernameTxt.getText().Trim());
            cartWindow.ShowDialog();
        }

        public static void inject(IAuthService _authService) {
            authService = _authService;
        }
    }
}
