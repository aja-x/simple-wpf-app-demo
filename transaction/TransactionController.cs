﻿using rasyidtechlib.basic;
using SimpeApp.service;

namespace SimpeApp.transaction {
    class TransactionController : MyController {
        private static IAuthService authService;

        public TransactionController(IMyView _myView) : base(_myView) {
        }

        public void doLogout() {
            authService.logout();
        }

        public static void inject(IAuthService _authService) {
            authService = _authService;
        }
    }
}
