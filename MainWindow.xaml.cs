﻿using SimpeApp.login;
using SimpeApp.register;
using SimpeApp.service;
using SimpeApp.transaction;
using System.Windows;

namespace SimpeApp {
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
            inject();
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.Show();
            Close();
        }

        private void inject() {
            IAuthService authService = AuthService.getInstance();
            LoginController.inject(authService);
            RegisterController.inject(authService);
            TransactionController.inject(authService);
            AddTransactionWindow.inject(authService);
        }
    }
}
