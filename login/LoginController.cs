﻿using rasyidtechlib.basic;
using rasyidtechlib.ui_element.password_box;
using rasyidtechlib.ui_element.text_box;
using SimpeApp.service;
using System.Windows;

namespace SimpeApp.login {
    class LoginController : MyController {
        private static IAuthService authService;

        public LoginController(IMyView _myView) : base(_myView) {
        }

        public void doLogin(IMyTextBox username, IMyPasswordBox password) {
            bool loggedIn = authService.login(username.getText(), password.getPassword());
            if (! loggedIn) {
                MessageBox.Show("Something wrong with your username or password", "Login Failed", MessageBoxButton.OK);
                return;
            }

            if (authService.getCurrentUserInfo().getAccountType() == AccountType.KARYAWAN) {
                getView().callMethod("moveToTransactionWindow");
            } else {
                getView().callMethod("moveToCartWindow");
            }
        }

        public static void inject(IAuthService _authService) {
            authService = _authService;
        }
    }
}
