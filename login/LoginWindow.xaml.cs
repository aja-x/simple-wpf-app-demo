﻿using rasyidtechlib.basic;
using rasyidtechlib.ui_element.button;
using rasyidtechlib.ui_element.password_box;
using rasyidtechlib.ui_element.text_box;
using SimpeApp.register;
using SimpeApp.transaction;

namespace SimpeApp.login {
    public partial class LoginWindow : MyWindow, IMyController {
        private IMyTextBox usernameTxt;
        private IMyPasswordBox passwordTxt;
        private IMyButton loginBtn;
        private IMyButton registerBtn;

        public LoginWindow() {
            InitializeComponent();

            setController(new LoginController(this));
            usernameTxt = new BuilderTextBox().activate(this, "username");
            passwordTxt = new BuilderPasswordBox().activate(this, "password");

            loginBtn = new BuilderButton().activate(this, "loginButton").addOnClick(this, "onClickLoginButton");
            registerBtn = new BuilderButton().activate(this, "registerButton").addOnClick(this, "onClickRegisterButton");
        }

        public void onClickLoginButton() {
            getController().callMethod("doLogin", usernameTxt, passwordTxt);
        }

        public void onClickRegisterButton() {
            RegisterWindow registerWindow = new RegisterWindow();
            registerWindow.Show();
            hiddenWindow();
        }

        public void moveToTransactionWindow() {
            TransactionWindow transactionWindow = new TransactionWindow();
            transactionWindow.Show();
            hiddenWindow();
        }

        public void moveToCartWindow() {
            hiddenWindow();
        }

        private void hiddenWindow() {
            this.Close();
        }
    }
}
